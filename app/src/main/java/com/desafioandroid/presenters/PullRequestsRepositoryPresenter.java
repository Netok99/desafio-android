package com.desafioandroid.presenters;

import android.util.Log;

import com.desafioandroid.BuildConfig;
import com.desafioandroid.core.models.Item;
import com.desafioandroid.core.models.Pull;
import com.desafioandroid.core.presenters.IPullRequestsRepositoryPresenter;
import com.desafioandroid.gateways.GithubGateway;
import com.desafioandroid.ui.activities.PullRequestsActivity;

import java.util.List;

/**
 * Created by neto on 25/02/17.
 */

public class PullRequestsRepositoryPresenter implements IPullRequestsRepositoryPresenter {

    private final PullRequestsActivity activity;

    public PullRequestsRepositoryPresenter(PullRequestsActivity activity) {
        this.activity = activity;
    }

    @Override
    public void getPullRequestsRepository(Item item) {
        new GithubGateway().getPullRequestsRepository(
                item.getOwner().getLogin(),
                item.getName(),
                this);
    }

    @Override
    public void onError(String message) {
        activity.messageError();
        if (BuildConfig.DEBUG) {
            Log.e("getGithubRepositories", message);
        }
    }

    @Override
    public void onNextPull(List<Pull> pulls) {
        int open = 0;
        int closed = 0;
        for (Pull pull : pulls) {
            if (pull.getState().equals("open")) {
                open++;
            } else {
                closed++;
            }
        }
        activity.fillListPullRequests(pulls);
        activity.fillTopInformation(closed, open);
    }
}
