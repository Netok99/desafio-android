package com.desafioandroid.presenters;

import android.util.Log;

import com.desafioandroid.BuildConfig;
import com.desafioandroid.core.models.Repository;
import com.desafioandroid.core.presenters.IGithubRepositoriesPresenter;
import com.desafioandroid.gateways.GithubGateway;
import com.desafioandroid.ui.activities.GithubRepositoriesActivity;

/**
 * Created by neto on 23/02/17.
 */

public final class GithubRepositoriesPresenter implements IGithubRepositoriesPresenter {

    private final GithubRepositoriesActivity activity;

    public GithubRepositoriesPresenter(GithubRepositoriesActivity activity) {
        this.activity = activity;
    }

    @Override
    public void getGithubRepositories(int numberPagination) {
        new GithubGateway().getGithubRepositories(numberPagination, this);
    }

    @Override
    public void onError(String message) {
        activity.messageError();
        if (BuildConfig.DEBUG) {
            Log.e("getGithubRepositories", message);
        }
    }

    @Override
    public void onNext(Repository repository) {
        activity.addRepositoryList(repository);
    }
}
