package com.desafioandroid.gateways;

import com.desafioandroid.BuildConfig;
import com.desafioandroid.core.gateways.IGithubGateway;
import com.desafioandroid.core.models.Pull;
import com.desafioandroid.core.models.Repository;
import com.desafioandroid.core.presenters.IGithubRepositoriesPresenter;
import com.desafioandroid.core.presenters.IPullRequestsRepositoryPresenter;
import com.desafioandroid.networks.IGithubService;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by neto on 09/02/17.
 */

public final class GithubGateway implements IGithubGateway {

    private static final String ENDPOINT = "https://api.github.com/";

    private static IGithubService setupRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(IGithubService.class);

    }

    public void getGithubRepositories(final int numberPagination,
                                      final IGithubRepositoriesPresenter presenter) {
        Observable<Repository> github = setupRetrofit().getGithubRepositories(numberPagination);
        github.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Repository>() {

                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        presenter.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(Repository repository) {
                        presenter.onNext(repository);
                    }
                });
    }

    public void getPullRequestsRepository(final String repository, final String creator,
                                          final IPullRequestsRepositoryPresenter presenter) {
        Observable<List<Pull>> github = setupRetrofit().getPullRequestsRepository(repository, creator);
        github.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Pull>>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {
                        presenter.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Pull> pulls) {
                        presenter.onNextPull(pulls);
                    }
                });
    }
}
