package com.desafioandroid.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.desafioandroid.R;
import com.desafioandroid.core.models.Item;
import com.desafioandroid.databinding.ItemGithubBinding;
import com.desafioandroid.ui.activities.PullRequestsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neto on 23/02/17.
 */

public class GithubListAdapter extends RecyclerView.Adapter {

    private List<Item> items;
    private final Context context;

    public GithubListAdapter(Context context) {
        items = new ArrayList<>();
        this.context = context;
    }

    public void addItem(List<Item> items) {
        for (Item item : items) {
            this.items.add(item);
        }
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewGitHub((ItemGithubBinding) DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.item_github, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Item item = items.get(position);
        final RecyclerViewGitHub holderGithub = (RecyclerViewGitHub)holder;
        holderGithub.binding.setItem(item);
        configColorViews(holderGithub);
        holderGithub.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PullRequestsActivity.class);
                intent.putExtra("item", item);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void configColorViews(RecyclerViewGitHub holderGithub) {
        int color = ContextCompat.getColor(context, R.color.gamboge);
        holderGithub.binding.imgNumberForks.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        holderGithub.binding.imgNumberStars.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
    }
}

class RecyclerViewGitHub extends RecyclerView.ViewHolder {
    final ItemGithubBinding binding;

    RecyclerViewGitHub(ItemGithubBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
