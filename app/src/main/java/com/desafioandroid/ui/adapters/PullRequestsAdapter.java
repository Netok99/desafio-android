package com.desafioandroid.ui.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.desafioandroid.R;
import com.desafioandroid.core.models.Pull;
import com.desafioandroid.databinding.ItemPullRequestBinding;

import java.util.List;

/**
 * Created by neto on 25/02/17.
 */
public class PullRequestsAdapter extends RecyclerView.Adapter {

    private final List<Pull> pulls;

    public PullRequestsAdapter(List<Pull> pulls) {
        this.pulls = pulls;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewPullRequests((ItemPullRequestBinding) DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.item_pull_request, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Pull pull = pulls.get(position);
        final RecyclerViewPullRequests holderPullRequest = (RecyclerViewPullRequests) holder;
        holderPullRequest.binding.setPull(pull);
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }
}

class RecyclerViewPullRequests extends RecyclerView.ViewHolder {
    final ItemPullRequestBinding binding;

    RecyclerViewPullRequests(ItemPullRequestBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
