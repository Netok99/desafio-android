package com.desafioandroid.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import com.desafioandroid.R;
import com.desafioandroid.core.models.Repository;
import com.desafioandroid.core.presenters.IGithubRepositoriesPresenter;
import com.desafioandroid.databinding.GithubRepositoriesActivityBinding;
import com.desafioandroid.presenters.GithubRepositoriesPresenter;
import com.desafioandroid.ui.adapters.GithubListAdapter;
import com.desafioandroid.ui.viewUtils.EndlessRecyclerOnScrollListener;
import com.desafioandroid.ui.viewUtils.SimpleDividerItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GithubRepositoriesActivity extends BaseActivity {

    private GithubRepositoriesActivityBinding binding;
    private IGithubRepositoriesPresenter presenter;
    private GithubListAdapter adapter;
    private List<Repository> repositories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_github_repositories);
        presenter = new GithubRepositoriesPresenter(this);
        configViews();
        if (savedInstanceState == null) {
            getGithubRepositories(1);
        } else {
            List<Repository> repositories =
                    (List<Repository>) savedInstanceState.getSerializable("repositories");
            if (repositories != null) {
                for (Repository repository : repositories) {
                    addRepositoryList(repository);
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (!repositories.isEmpty()) {
            savedInstanceState.putSerializable("repositories", (Serializable) repositories);
        }
    }

    private void configViews() {
        configListEmployees();
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getGithubRepositories(1);
            }
        });
    }

    private void configListEmployees() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.listGithub.setLayoutManager(linearLayoutManager);
        binding.listGithub.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                presenter.getGithubRepositories(currentPage);
                binding.swipeRefresh.setRefreshing(true);
            }
        });
        binding.listGithub.setHasFixedSize(true);
        binding.listGithub.addItemDecoration(new SimpleDividerItemDecoration(this));
        adapter = new GithubListAdapter(this);
        binding.listGithub.setAdapter(adapter);
    }

    private void getGithubRepositories(int numberPagination) {
        if (isOnline(this)) {
            binding.swipeRefresh.setRefreshing(true);
            presenter.getGithubRepositories(numberPagination);
        } else {
            messageError();
        }
    }

    public void addRepositoryList(Repository repository) {
        repositories.add(repository);
        adapter.addItem(repository.getItems());
        binding.swipeRefresh.setRefreshing(false);
    }

    public void messageError() {
        binding.swipeRefresh.setRefreshing(false);
        Toast.makeText(this, getString(R.string.error_internet), Toast.LENGTH_LONG).show();
    }
}
