package com.desafioandroid.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import com.desafioandroid.R;
import com.desafioandroid.core.models.Item;
import com.desafioandroid.core.models.Pull;
import com.desafioandroid.core.presenters.IPullRequestsRepositoryPresenter;
import com.desafioandroid.databinding.PullRequestsActivityBinding;
import com.desafioandroid.presenters.PullRequestsRepositoryPresenter;
import com.desafioandroid.ui.adapters.PullRequestsAdapter;
import com.desafioandroid.ui.viewUtils.SimpleDividerItemDecoration;

import java.io.Serializable;
import java.util.List;

/**
 * Created by neto on 25/02/17.
 */

public class PullRequestsActivity extends BaseActivity {

    private PullRequestsActivityBinding binding;
    private Item item;
    private IPullRequestsRepositoryPresenter presenter;
    private List<Pull> pulls;
    private int closed;
    private int open;
    public static final String ITEM = "item";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pull_requests);
        item = (Item) getIntent().getSerializableExtra(ITEM);
        presenter = new PullRequestsRepositoryPresenter(this);
        configViews();
        if (savedInstanceState == null) {
            getPullRequestsRepository();
        } else {
            fillListPullRequests((List<Pull>) savedInstanceState.getSerializable("pulls"));
            fillTopInformation(savedInstanceState.getInt("closed"), savedInstanceState.getInt("open"));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable("pulls", (Serializable)pulls);
        savedInstanceState.putInt("closed", closed);
        savedInstanceState.putInt("open", open);
    }

    private void configViews() {
        configToolBar(true, item.getName());
        binding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPullRequestsRepository();
            }
        });
    }

    private void getPullRequestsRepository() {
        if (isOnline(this)) {
            binding.swipeRefresh.setRefreshing(true);
            presenter.getPullRequestsRepository(item);
        } else {
            messageError();
        }
    }

    public void fillListPullRequests(List<Pull> pulls) {
        this.pulls = pulls;
        binding.swipeRefresh.setRefreshing(false);
        binding.listPullRequests.setLayoutManager(new LinearLayoutManager(this));
        binding.listPullRequests.setHasFixedSize(true);
        binding.listPullRequests.addItemDecoration(new SimpleDividerItemDecoration(this));
        binding.listPullRequests.setAdapter(new PullRequestsAdapter(pulls));
    }

    public void fillTopInformation(int closed, int open) {
        this.closed = closed;
        this.open = open;
        binding.txtOpened.setText(open + " opened");
        binding.txtClosed.setText(" / " + closed + " closed");
    }

    public void messageError() {
        binding.swipeRefresh.setRefreshing(false);
        Toast.makeText(this, getString(R.string.error_internet), Toast.LENGTH_LONG).show();
    }
}
