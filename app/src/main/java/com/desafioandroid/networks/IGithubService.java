package com.desafioandroid.networks;

import com.desafioandroid.core.models.Pull;
import com.desafioandroid.core.models.Repository;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by neto on 09/02/17.
 */

public interface IGithubService {

    @GET("search/repositories?q=language:Java&sort=stars&")
    Observable<Repository> getGithubRepositories(@Query("page") int numberPagination);

    @GET("repos/{repository}/{creator}/pulls")
    Observable<List<Pull>> getPullRequestsRepository(@Path("repository") String repository,
                                                     @Path("creator") String creator);

}
