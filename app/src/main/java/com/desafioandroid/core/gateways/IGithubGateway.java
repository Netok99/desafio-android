package com.desafioandroid.core.gateways;


import com.desafioandroid.core.presenters.IGithubRepositoriesPresenter;
import com.desafioandroid.core.presenters.IPullRequestsRepositoryPresenter;

/**
 * Created by neto on 12/02/17.
 */

public interface IGithubGateway {

    void getGithubRepositories(int numberResults, final IGithubRepositoriesPresenter presenter);

    void getPullRequestsRepository(final String creator, String repository,
                                   final IPullRequestsRepositoryPresenter presenter);
}
