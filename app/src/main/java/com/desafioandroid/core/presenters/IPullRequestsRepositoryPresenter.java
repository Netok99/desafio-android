package com.desafioandroid.core.presenters;

import com.desafioandroid.core.models.Item;
import com.desafioandroid.core.models.Pull;

import java.util.List;

/**
 * Created by neto on 25/02/17.
 */

public interface IPullRequestsRepositoryPresenter {
    void getPullRequestsRepository(Item item);

    void onError(String message);

    void onNextPull(List<Pull> pull);
}
