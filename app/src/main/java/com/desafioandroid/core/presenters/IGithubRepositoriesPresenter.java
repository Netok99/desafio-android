package com.desafioandroid.core.presenters;

import com.desafioandroid.core.models.Repository;

/**
 * Created by neto on 23/02/17.
 */

public interface IGithubRepositoriesPresenter {
    void getGithubRepositories(int numberPagination);

    void onError(String message);

    void onNext(Repository repository);
}
