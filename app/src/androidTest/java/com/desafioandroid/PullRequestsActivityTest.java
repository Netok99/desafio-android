package com.desafioandroid;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.desafioandroid.core.models.Item;
import com.desafioandroid.core.models.Owner;
import com.desafioandroid.ui.activities.PullRequestsActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by neto on 27/02/17.
 */

@RunWith(AndroidJUnit4.class)
public class PullRequestsActivityTest {

    private MockWebServer server;

    @Rule
    public final ActivityTestRule<PullRequestsActivity>
            mActivityRule = new ActivityTestRule<>(PullRequestsActivity.class, false, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        setupServerUrl();
    }

    private void setupServerUrl() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        server.url("https://api.github.com/repos/elastic/elasticsearch/pulls");
    }

    @Test
    public void whenResultIsOk_shouldDisplayListWithRepositories() {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(Mock.SUCCESS_PULL));
        mActivityRule.launchActivity(createIntent());
        onView(withId(R.id.list_pull_requests)).check(matches(isDisplayed()));
    }

    private Intent createIntent() {
        return new Intent().putExtra(PullRequestsActivity.ITEM, new Item(new Owner()));
    }

    @Test
    public void checkUserItemView_isDisplayed() {
        server.enqueue(new MockResponse().setResponseCode(200).setBody(Mock.SUCCESS_PULL));
        mActivityRule.launchActivity(createIntent());
        onView(allOf(withId(R.id.txt_name), withText("jimczi/elasticsearch"))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.img_user), hasSibling(withText("jimczi")))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.txt_username), withText("jimczi"))).check(matches(isDisplayed()));
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}
