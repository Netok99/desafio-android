package com.desafioandroid;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.desafioandroid.ui.activities.GithubRepositoriesActivity;
import com.desafioandroid.ui.activities.PullRequestsActivity;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static android.support.test.espresso.matcher.ViewMatchers.hasSibling;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by neto on 27/02/17.
 */

@RunWith(AndroidJUnit4.class)
public class GithubRepositoriesActivityTest {

    private MockWebServer server;

    @Rule
    public final ActivityTestRule<GithubRepositoriesActivity>
            mActivityRule = new ActivityTestRule<>(GithubRepositoriesActivity.class, false, false);

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
        setupServerUrl();
    }

    private void setupServerUrl() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        server.url("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1");
    }

    @Test
    public void whenResultIsOk_shouldDisplayListWithRepositories() {
        server.enqueue(new MockResponse().setResponseCode(200));
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.list_github)).check(matches(isDisplayed()));
    }

    @Test
    public void checkUserItemView_isDisplayed() {
        server.enqueue(new MockResponse().setResponseCode(200));
        mActivityRule.launchActivity(new Intent());
        onView(allOf(withId(R.id.txt_name), withText("elasticsearch"))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.img_user), hasSibling(withText("elastic")))).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.txt_username), withText("elastic"))).check(matches(isDisplayed()));
    }

    @Test
    public void whenClickOnItemList_shouldStartUserDetailsActivity_withExtra() {
        server.enqueue(new MockResponse().setResponseCode(200));
        mActivityRule.launchActivity(new Intent());
        Intents.init();

        Matcher<Intent> matcher = allOf(
                hasComponent(PullRequestsActivity.class.getName()),
                hasExtraWithKey(PullRequestsActivity.ITEM)
        );

        Instrumentation.ActivityResult
                result = new Instrumentation.ActivityResult(Activity.RESULT_OK, null);
        intending(matcher).respondWith(result);
        onView(withId(R.id.list_github)).perform(actionOnItemAtPosition(0, click()));
        intended(matcher);
        Intents.release();
    }

    @After
    public void tearDown() throws IOException {
        server.shutdown();
    }
}
